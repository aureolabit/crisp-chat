import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import './styles/index.sass';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
