import React, { useEffect } from 'react';
import Logo from '../assets/images/logo.png';

const App = () => {
  useEffect(() => {
    window.$crisp = [];
    window.CRISP_WEBSITE_ID = process.env.REACT_APP_CRISP;
    (function() {
      var d = document;
      var s = d.createElement('script');
      s.src = 'https://client.crisp.chat/l.js';
      s.async = 1;
      d.getElementsByTagName('head')[0].appendChild(s);
    })();
  }, [])

  return (
    <div>
      <img src={Logo} alt="Aureolab" />
    </div>
  );
};

export default App;
